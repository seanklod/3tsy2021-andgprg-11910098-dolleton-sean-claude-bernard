using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public static WaveManager instance;

    // ground mobs
    public GameObject goblin;
    public GameObject pig;
    public GameObject spider;
    public GameObject slime;
    public GameObject bossGoblin;
    public GameObject bossKoko;

    // flying mobs
    public GameObject bossDragon;
    public GameObject raven;

    public Transform spawnPoint;


    public int waveCount = 1;
    public float nextWaveCountdown = 10f;
    public float timeBetweenWaves = 10f;

    public float timeBetweenSpawns = 3f;
    public float spawnCountdown = 3;
    bool waveFinished = false; // wave is considered finished if all mobs are gone
    bool doneSpawning = false;

    int mobCount = 0;
    public int mobAlive = 0;

    private void Start()
    {
        instance = this;
        UIMgr.instance.UpdateWaveCount(waveCount);
        UIMgr.instance.updateMobAliveCount(mobAlive);
    }

    // Update is called once per frame
    void Update()
    {
        // wave will start if countdown is equal or less 0, and if wave hasnt finished yet
        if (nextWaveCountdown <= 0f && !waveFinished)
        {
            if (spawnCountdown <= 0f)
            {
                startSpawning();
                spawnCountdown = timeBetweenSpawns;
            }
            spawnCountdown -= Time.deltaTime;
        }

        // if wave is done spawning & all mobs were killed, start countdown before next wave
        if (waveFinished && mobAlive == 0)
        {
            // reset the countdown to assigned value in timeBetweenWaves
            Debug.Log("Start countdown for next wave!");
            nextWaveCountdown = timeBetweenWaves;

            // set these back to default
            waveFinished = false;
            doneSpawning = false;
        }

        nextWaveCountdown -= Time.deltaTime; // computes countdown in seconds

        UIMgr.instance.UpdateWaveCount(waveCount);
        UIMgr.instance.updateMobAliveCount(mobAlive);
    }


    public void startSpawning()
    {
        // spawn wave 1
        if (waveCount == 1 && !doneSpawning)
        {
            if (mobCount < 25)
            {
                StartCoroutine(spawnWave1());
            }

            if (mobCount >= 25)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 2 && !doneSpawning)
        {
            Debug.Log("START WAVE 2");
            if (mobCount < 31)
            {
                StartCoroutine(spawnWave2());
            }

            if (mobCount >= 30)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 3 && !doneSpawning)
        {
            Debug.Log("START WAVE 3");
            if (mobCount < 36)
            {
                StartCoroutine(spawnWave3());
            }

            if (mobCount >= 35)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 4 && !doneSpawning)
        {
            Debug.Log("START WAVE 4");
            if (mobCount < 26)
            {
                StartCoroutine(spawnWave4());
            }

            if (mobCount >= 25)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 5 && !doneSpawning)
        {
            Debug.Log("START WAVE 5");
            if (mobCount < 26)
            {
                StartCoroutine(spawnWave5());
            }

            if (mobCount >= 25)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 6 && !doneSpawning)
        {
            Debug.Log("START WAVE 6");
            if (mobCount < 36)
            {
                StartCoroutine(spawnWave6());
            }

            if (mobCount >= 35)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 7 && !doneSpawning)
        {
            Debug.Log("START WAVE 7");
            if (mobCount < 26)
            {
                StartCoroutine(spawnWave7());
            }

            if (mobCount >= 25)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 8 && !doneSpawning)
        {
            Debug.Log("START WAVE 8");
            if (mobCount < 36)
            {
                StartCoroutine(spawnWave8());
            }

            if (mobCount >= 35)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 9 && !doneSpawning)
        {
            Debug.Log("START WAVE 9");
            if (mobCount < 36)
            {
                StartCoroutine(spawnWave9());
            }

            if (mobCount >= 35)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        if (waveCount == 10 && !doneSpawning)
        {
            Debug.Log("START WAVE 10");
            if (mobCount < 26)
            {
                StartCoroutine(spawnWave10());
            }

            if (mobCount >= 25)
            {
                doneSpawning = true; // if done spawning all number of mobs, set to true to stop spawning
            }
        }

        updateWaveStatus();
    }

    void updateWaveStatus()
    {
        // if all mobs are gone,
        if (mobAlive <= 0)
        {
            Debug.Log("Wave " + waveCount + " has finished");
            waveCount++; // increment wave index
            mobCount = 0; // reset spawner counter
            waveFinished = true; // set to true to start nextwave cooldown
        }
    }

    IEnumerator spawnWave1()
    {
        if (mobCount < 15) // spawn 14 goblins
        {
            SpawnGoblins();
        }

        if (mobCount >= 15) // spawn 11 pigs
        {
            SpawnPigs();
        }

        mobAlive++;
        mobCount++;

        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave2()
    {
        if (mobCount <= 15) // spawn 15 goblins
        {
            SpawnGoblins();
        }

        if (mobCount > 15 && mobCount < 26) // spawn 10 slimes
        {
            SpawnSlimes();
        }

        if (mobCount >= 26) // spawn 5 ravens
        {
            SpawnRavens();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave3()
    {
        if (mobCount <= 15) // spawn 15 goblins
        {
            SpawnGoblins();
        }

        if (mobCount > 15 && mobCount < 26) // spawn 10 pigs
        {
            SpawnRavens();
        }

        if (mobCount >= 26) // spawn 10 spiders
        {
            SpawnSpiders();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave4()
    {
        if (mobCount <= 15) // spawn 15 spiders
        {
            SpawnSpiders();
        }

        if (mobCount > 15 && mobCount < 21) // spawn 5 ravens
        {
            SpawnRavens();
        }

        if (mobCount >= 21 && mobCount < 25) // spawn 4 goblins
        {
            SpawnGoblins();
        }

        if (mobCount >= 24 && mobCount < 26) // spawn boss goblin
        {
            SpawnBossGoblin();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave5()
    {
        if (mobCount <= 15) // spawn 15 spiders
        {
            SpawnSpiders();
        }

        if (mobCount > 15 && mobCount < 21) // spawn 5 ravens
        {
            SpawnRavens();
        }

        if (mobCount >= 21 && mobCount < 26) // spawn 5 goblins
        {
            SpawnGoblins();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave6()
    {
        if (mobCount <= 15) // spawn 15 goblins
        {
            SpawnGoblins();
        }

        if (mobCount > 15 && mobCount < 26) // spawn 10 ravens
        {
            SpawnRavens();
        }

        if (mobCount >= 26) // spawn 10 spiders
        {
            SpawnSpiders();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave7()
    {
        if (mobCount <= 15) // spawn 15 ravens
        {
            SpawnRavens();
        }

        if (mobCount > 15 && mobCount < 21) // spawn 5 slimes
        {
            SpawnSlimes();
        }

        if (mobCount >= 21 && mobCount < 25) // spawn 4 goblins
        {
            SpawnGoblins();
        }

        if (mobCount >= 24 && mobCount < 26) // spawn boss koko
        {
            SpawnBossKoko();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave8()
    {
        if (mobCount <= 15) // spawn 15 goblins
        {
            SpawnGoblins();
        }

        if (mobCount > 15 && mobCount < 26) // spawn 10 ravens
        {
            SpawnRavens();
        }

        if (mobCount >= 26) // spawn 10 pigs
        {
            SpawnPigs();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave9()
    {
        if (mobCount <= 15) // spawn 15 goblins
        {
            SpawnGoblins();
        }

        if (mobCount > 15 && mobCount < 26) // spawn 10 ravens
        {
            SpawnRavens();
        }

        if (mobCount >= 26) // spawn 10 pigs
        {
            SpawnPigs();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator spawnWave10()
    {
        if (mobCount <= 15) // spawn 15 spiders
        {
            SpawnSpiders();
        }

        if (mobCount > 15 && mobCount < 21) // spawn 5 ravens
        {
            SpawnRavens();
        }

        if (mobCount >= 21 && mobCount < 25) // spawn 4 goblins
        {
            SpawnGoblins();
        }

        if (mobCount >= 24 && mobCount < 26) // spawn boss dragon
        {
            SpawnBossDragon();
        }

        mobAlive++;
        mobCount++;
        yield return new WaitForSeconds(3f);
    }

     void SpawnGoblins()
    {  
        Instantiate(goblin, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }

    void SpawnRavens()
    {
        Instantiate(raven, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }

    void SpawnPigs()
    {
        Instantiate(pig, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }

    void SpawnSlimes()
    {
        Instantiate(slime, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }

    void SpawnSpiders()
    {
        Instantiate(spider, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }

    void SpawnBossGoblin()
    {
        Instantiate(bossGoblin, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }

    void SpawnBossKoko()
    {
        Instantiate(bossKoko, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }

    void SpawnBossDragon()
    {
        Instantiate(bossDragon, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }
}
