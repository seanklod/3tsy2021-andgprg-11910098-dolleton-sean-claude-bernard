using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    float moveSpeed = 5.0f;

    // Update is called once per frame
    void Update()
    {
        moveCamera();
    }

    void moveCamera()
    {
        // move left
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.position -= new Vector3(0, 0, moveSpeed) * Time.deltaTime;
        }

        // move right
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.position += new Vector3(0, 0, moveSpeed) * Time.deltaTime;
        }

        // move forward
        if (Input.GetKey(KeyCode.W))
        {
            this.transform.position -= new Vector3(moveSpeed, 0, 0) * Time.deltaTime;
        }

        // move backwards
        if (Input.GetKey(KeyCode.S))
        {
            this.transform.position += new Vector3(moveSpeed, 0, 0) * Time.deltaTime;
        }
    }

}
