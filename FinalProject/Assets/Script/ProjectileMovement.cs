using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    public bool canDamageFlying;
    private Transform target;
    public float speed = 50f;
    public float explosionRadius;
    public float damage;
    public float burningDmg;
    public float slowAmount;
    public float debuffDuration;

    public void chaseTarget(Transform whatTarget)
    {
        target = whatTarget;
    }

    public void inheritTowerStats(float  dmg, float explosionArea, float burnDamage, float iceSlow, float debuffTime)
    {
        damage = dmg;
        explosionRadius = explosionArea;
        burningDmg = burnDamage;
        slowAmount = iceSlow;
        debuffDuration = debuffTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 direction = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (direction.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(direction.normalized * distanceThisFrame, Space.World);
    }

    void HitTarget()
    {
        if (explosionRadius > 0f)
        {
            Explode();
        }
        else
        {
            Damage(target);
        }

        // destroy bullet upon hitting enemy
        Destroy(gameObject);
    }

    void Explode()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider hitCollider in hitColliders)
        {
            if (hitCollider.tag == "Enemy")
            {
                if (hitCollider.gameObject.GetComponent<EnemyStats>().isFlyingMob == true && canDamageFlying == false)
                { 
                    // ignore
                }
                else
                {
                    Damage(hitCollider.transform);
                }
            }
        }
    }

    void Damage(Transform enemy)
    {
        if (this.gameObject.name == "CannonBall(Clone)")
        {
            Debug.Log("hit with cannonball");
            enemy.gameObject.GetComponent<EnemyStats>().takeDamage(damage);
        }

        if (this.gameObject.name == "BowArrow(Clone)")
        {
            enemy.gameObject.GetComponent<EnemyStats>().takeDamage(damage);
        }

        if (this.gameObject.name == "FireBullet(Clone)")
        {
            enemy.gameObject.GetComponent<EnemyStats>().takeDamage(damage);
            enemy.gameObject.GetComponent<EnemyMovement>().burning(debuffDuration, burningDmg);
        }

        if (this.gameObject.name == "IceBullet(Clone)")
        {
            enemy.gameObject.GetComponent<EnemyStats>().takeDamage(damage);
            enemy.gameObject.GetComponent<EnemyMovement>().slowed(debuffDuration, slowAmount);
        }
    }

    private void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
