using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform target;

    public bool isSlowed = false;
    public bool isBurning = false;

    float slowDuration;
    float slowAmount;
    float durationCountdown;

    float burningDuration;
    float burnAmount;
    float secondCounter = 1f;

    void OnCollisionEnter(Collision collision)
    {
        // destroy spawn upon contact with Core/Player Base
        if (collision.gameObject.CompareTag("Player"))
        {
            // decrement mobAlive in WaveManager everytime this obj is destroyed by either towers or after getting in contact with the core
            WaveManager.instance.mobAlive--;
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(target.position);

        if (isSlowed)
        {
            if (durationCountdown <= 0f)
            {
                agent.speed += slowAmount;
                isSlowed = false;
            }

            durationCountdown -= Time.deltaTime;
        }

        if (isBurning)
        {
            if (burningDuration > 0f)
            {
                if (secondCounter <= 0)
                {
                    this.gameObject.GetComponent<EnemyStats>().takeDamage(burnAmount);
                    secondCounter = 1f;
                    burningDuration--;
                }

                secondCounter -= Time.deltaTime;
            }

            else
            {
                isBurning = false;
            }
        }
    }

    // these 2 will be called by ProjectileMovement.cs
    public void slowed(float duration, float intensity)
    {
        if (!isSlowed)
        {
            Debug.Log("I am slowed");
            this.GetComponent<NavMeshAgent>().speed -= intensity; // deduct only once, slow amount wont stack
            slowAmount = intensity;
            isSlowed = true;
        }

        slowDuration = duration; // duration can refresh
        durationCountdown = slowDuration;
    }

    public void burning(float duration, float intensity)
    {
        burnAmount = intensity;
        burningDuration = duration; // allow burning effect to refresh
        isBurning = true;
    }
}