using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public static PlayerStats instance;

    int currentHp;
    public int coreHP = 250;
    public int currentGold = 500;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        currentHp = coreHP;

        UIMgr.instance.UpdatePlayerHealth(currentHp, coreHP);
        UIMgr.instance.UpdateGold(currentGold);
    }

    void OnCollisionEnter(Collision collision)
    {
        // deduct hp when enemies get to base
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (collision.gameObject.GetComponent<EnemyStats>().isBoss)
            {
                currentHp = 0;

            }
            
            else if (collision.gameObject.GetComponent<EnemyStats>().isBoss == false)
            {
                currentHp -= 1;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        checkHP();
        UIMgr.instance.UpdatePlayerHealth(currentHp, coreHP);
        UIMgr.instance.UpdateGold(currentGold);
    }

    void checkHP()
    {
        // put code here
        if (currentHp <= 0)
        {
            LevelManager.instance.loadDeath();
        }
    }

    public void addGold(int mobGold)
    {
        currentGold += mobGold;
    }
}
