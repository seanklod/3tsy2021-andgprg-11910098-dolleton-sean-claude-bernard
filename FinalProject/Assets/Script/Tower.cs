﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public MeshRenderer meshRenderer;
    public Material originalMaterial;
    public Material redMaterial;
    public Material greenMaterial;
    public Material blackMaterial;

    public SkinnedMeshRenderer skinnedMesh;

    public float buildTimer;
    public float buildCountdown;
    public bool startBuilding = false;

    private void Start()
    {
        buildTimer = buildCountdown;
        this.GetComponent<TowerStatsBehavior>().enabled = false;
        UIMgr.instance.updateBuildTime(buildCountdown);
    }

    private void Update()
    {
        if (startBuilding)
        {
            // only run if countdown is done
            if (buildCountdown <= 0f)
            {
                meshRenderer.material = originalMaterial;
                this.GetComponent<TowerStatsBehavior>().enabled = true;
                UIMgr.instance.updateBuildTime(0f);
            }

            if (buildCountdown > 0)
            {
                UIMgr.instance.updateBuildTime(buildCountdown);
                buildCountdown -= Time.deltaTime;
            }
        }


        else { return;  }
    }

    public void Build()
    {
        // upon building, set color to black and disable any targeting & attacking scripts
        if (this.gameObject.name != "Crossbow1")
        {
            meshRenderer.material = blackMaterial;
            startBuilding = true;
        }

        if (this.gameObject.name == "Crossbow1") // does the same but for the crossbow
        {
            skinnedMesh.material = blackMaterial;
            startBuilding = true;
        }

        this.GetComponent<Collider>().enabled = true;
    }

    public void NonBuildable()
    {
        if (this.gameObject.name != "Crossbow1")
        {
            meshRenderer.material = redMaterial;
        }

        if (this.gameObject.name == "Crossbow1")
        {
            skinnedMesh.material = redMaterial;
        }
    }

    public void Buildable()
    {
        if (this.gameObject.name != "Crossbow1")
        {
            meshRenderer.material = greenMaterial;
        }

        if (this.gameObject.name == "Crossbow1")
        {
            skinnedMesh.material = greenMaterial;
        }
    }
}
