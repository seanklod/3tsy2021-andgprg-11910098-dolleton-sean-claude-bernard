using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerStatsBehavior : MonoBehaviour
{
    [Header("Unity Setup")]
    private Transform target;
    public Transform partToRotate;
    public GameObject bulletPrefab;
    public Transform nozzle;

    [Header("Tower Stats")]
    public bool canTargetFlying;
    public float damage;
    public float range = 15f;
    public float fireRate = 2f;
    private float fireCountdown = 0f;
    public float explosionRadius;
    public float burningDamage;
    public float slowAmount;
    public float debuffDuration;

    public int cost;
    public int upgradeCost;
    public int upgradeLevel = 1;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.3f);
        UIMgr.instance.showTowerInfo(damage, fireRate, range, explosionRadius, slowAmount, burningDamage, debuffDuration);
        UIMgr.instance.showBasicTowerInfo(this.gameObject.name, cost, upgradeCost, upgradeLevel);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }    
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        } 
       
        else
        {
            target = null;
        }
    }

    void lookAtTarget()
    {
        Vector3 pointDirection = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(pointDirection);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * 10f).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null) return;

        // look at direction of target
        lookAtTarget();

        if (fireCountdown <= 0f)
        {
            // if enemy is flying & tower cant target it, dont do anything
            if (target.gameObject.GetComponent<EnemyStats>().isFlyingMob == true && this.canTargetFlying == false)
            {
                // ignore
            }

            
            else
            {
                Shoot();
            }

            fireCountdown = 1f / fireRate;
        }

        fireCountdown -= Time.deltaTime;
    }

    void Shoot()
    {
        GameObject bulletObject =  (GameObject)Instantiate(bulletPrefab, nozzle.position, nozzle.rotation);
        ProjectileMovement bullet = bulletObject.GetComponent<ProjectileMovement>();

        if (bullet != null)
        {
           bullet.chaseTarget(target);
           bullet.inheritTowerStats(damage, explosionRadius, burningDamage, slowAmount, debuffDuration);
        }

    }

    public void upgradeTower()
    {
        if (upgradeLevel < 5 && upgradeCost <= PlayerStats.instance.currentGold)
        {
            if (this.gameObject.name == "Cannon1(Clone)")
            {
                damage += (damage * 0.2f);
                range += 1;
                fireRate += 0.1f;
                explosionRadius += 0.1f;
            }

            if (this.gameObject.name == "Crossbow1(Clone)")
            {
                damage += (damage * 0.2f);
                range += 1;
                fireRate += 0.1f;
            }

            if (this.gameObject.name == "IceTower1(Clone)")
            {
                damage += (damage * 0.2f);
                range += 1;
                fireRate += 0.1f;
                explosionRadius += 0.1f;
                slowAmount += 0.2f;
                debuffDuration += 2;
            }

            if (this.gameObject.name == "FireTower1(Clone)")
            {
                damage += (damage * 0.2f);
                range += 1;
                fireRate += 0.1f;
                explosionRadius += 0.1f;
                burningDamage += 5f;
                debuffDuration += 2;
            }

            PlayerStats.instance.currentGold -= upgradeCost;
            upgradeLevel++;
            upgradeCost += 30;
        }


        printTowerInfo();
    }

    public void printTowerInfo()
    {
        UIMgr.instance.showTowerInfo(damage, fireRate, range, explosionRadius, slowAmount, burningDamage, debuffDuration);
        UIMgr.instance.showBasicTowerInfo(this.gameObject.name, cost, upgradeCost, upgradeLevel);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
