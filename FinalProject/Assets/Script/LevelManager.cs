using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    private void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.GetComponent<WaveManager>().waveCount >= 10)
        {
            SceneManager.LoadScene("Victory");
        }
    }

    public void loadDeath()
    {
        SceneManager.LoadScene("GameOver");
    }    
}
