using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIMgr : MonoBehaviour
{
    public static UIMgr instance;

    public TextMeshProUGUI hpText;
    public TextMeshProUGUI goldText;
    public TextMeshProUGUI waveNumberTxt;
    public TextMeshProUGUI mobAliveCountTxt;

    public TextMeshProUGUI nameTxt;
    public TextMeshProUGUI costTxt;
    public TextMeshProUGUI upgradeTxt;
    public TextMeshProUGUI levelTxt;
    public TextMeshProUGUI damageTxt;
    public TextMeshProUGUI fireRateTxt;
    public TextMeshProUGUI rangeTxt;
    public TextMeshProUGUI explosionRadTxt;
    public TextMeshProUGUI slowTxt;
    public TextMeshProUGUI burnTxt;
    public TextMeshProUGUI debuffTxt;
    public TextMeshProUGUI buildTimeTxt;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdatePlayerHealth(float currentHp, float maxHp)
    {
        hpText.text = currentHp + " / " + maxHp;
    }

    public void UpdateGold(int currentGold)
    {
        goldText.text = "Gold: " + currentGold;
    }

    public void UpdateWaveCount(int wavecount)
    {
        waveNumberTxt.text = "Wave: " + wavecount;

    }

    public void updateMobAliveCount(int mobCount)
    {
        mobAliveCountTxt.text = "Mobs Alive: " + mobCount;
    }

    public void showTowerInfo(float damage, float fireRate, float range, float explosionRadius, float slow, float burn, float time)
    {
        damageTxt.text = "Damage: " + damage;
        fireRateTxt.text = "Fire rate: " + fireRate;
        rangeTxt.text = "Range: " + range;
        explosionRadTxt.text = "AOE: " + explosionRadius;
        slowTxt.text = "Slow effect: -" + slow;
        burnTxt.text = "Burn effect: " + burn + "/s";
        debuffTxt.text = "Duration: " + time;
    }

    public void showBasicTowerInfo(string name, int cost, int upgradeCost, int level)
    {
        nameTxt.text = name;
        levelTxt.text = "Level: " + level;
        costTxt.text = "Cost: " + cost;
        upgradeTxt.text = "Upgrade cost: " + upgradeCost;
    }

    public void updateBuildTime(float time)
    {
        buildTimeTxt.text = "Building: " + time;

    }
}
