﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    GameObject currentTower;
    public int towerType = 0;

    public GameObject cannonPrefab;
    public GameObject arrowPrefab;
    public GameObject icePrefab;
    public GameObject firePrefab;

    private int playerGold;
    private bool canAfford;

    private int cannonCost = 200;
    private int arrowCost = 100;
    private int iceCost = 500;
    private int fireCost = 500;

    void Awake()
    {
        cannonPrefab.SetActive(false);
        arrowPrefab.SetActive(false);
        icePrefab.SetActive(false);
        firePrefab.SetActive(false);
    }

    float minYAxis = 2;
    float maxYAxis = 2.1f;
    float minYBuildArea = 2;

    Vector3 SnapToGrid(Vector3 towerObjectPos)
    {
        return new Vector3(Mathf.Round(towerObjectPos.x),
                            Mathf.Clamp(towerObjectPos.y, minYAxis, maxYAxis),
                            Mathf.Round(towerObjectPos.z));
    }

    void Update()
    {
        playerGold = PlayerStats.instance.currentGold;
        changeCurrentTower();

        if (towerType != 0 && canAfford)
        {
            BuildMode();
        }
    }

    public void BuildMode()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            currentTower.transform.position = SnapToGrid(hit.point);
            Debug.DrawLine(ray.origin, hit.point, Color.green);

            if (hit.point.y < minYBuildArea)
            {
                currentTower.GetComponent<Tower>().NonBuildable();
                return;
            }

            if (hit.collider.gameObject.tag == "Tower")
            {
                currentTower.GetComponent<Tower>().NonBuildable();
                return;
            }
            else
            {
                currentTower.GetComponent<Tower>().Buildable();
            }

            if (Input.GetMouseButtonDown(1)) // press rightclick to start building
            {
                GameObject tempTower = (GameObject)Instantiate(currentTower, SnapToGrid(hit.point), Quaternion.identity);
                tempTower.GetComponent<Tower>().Build();

                deductGold();
                currentTower.transform.position = new Vector3(0, 0, 0);

                currentTower = null;
                towerType = 0;
            }
        }
    }

    void deductGold()
    {
        if (currentTower == cannonPrefab)
        {
            PlayerStats.instance.currentGold -= cannonCost;
        }

        if (currentTower == arrowPrefab)
        {
            PlayerStats.instance.currentGold -= arrowCost;
        }

        if (currentTower == icePrefab)
        {
            PlayerStats.instance.currentGold -= iceCost;
        }

        if (currentTower == firePrefab)
        {
            PlayerStats.instance.currentGold -= fireCost;
        }

    }

    public void changeTowerIndex(int towerIndex)
    {
        towerType = towerIndex;
    }

    public void changeCurrentTower()
    {
        // if button 1 is pressed, select cannon
        if (towerType == 1)
        {
            if (cannonCost > playerGold)
            {
                Debug.Log("You cant afford this!");
                canAfford = false;
            }

            else
            {
                canAfford = true;
                cannonPrefab.SetActive(true);
                currentTower = cannonPrefab;
            }
        }

        // if button 2 is pressed, select arrow 
        if (towerType == 2)
        {
            if (arrowCost > playerGold)
            {
                canAfford = false;
            }
            else
            {
                canAfford = true;
                arrowPrefab.SetActive(true);
                currentTower = arrowPrefab;  
            }
        }

        // if button 3 is pressed, select ice tower
        if (towerType == 3)
        {
            if (iceCost > playerGold)
            {
                canAfford = false;
            }
            else
            {
                canAfford = true;
                icePrefab.SetActive(true);
                currentTower = icePrefab;
            }
        }

        // if button 4 is pressed, select fire tower
        if (towerType == 4)
        {
            if (fireCost > playerGold)
            {
                canAfford = false;
            }
            else
            {
                canAfford = true;
                firePrefab.SetActive(true);
                currentTower = firePrefab;
            }
        }
    }


}
