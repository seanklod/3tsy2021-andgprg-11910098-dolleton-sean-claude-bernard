using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    public int gold;
    public float maxHp;
    public float currentHp;

    public bool isFlyingMob;
    public bool isBoss;
    
    // Start is called before the first frame update
    void Start()
    {
        if (isBoss)
        {
            Debug.Log("spawned a boss");
            maxHp = GlobalEnemyStats.instance.maxHp + 600;
            gold = GlobalEnemyStats.instance.gold + 1000;
        }

        if (!isBoss)
        {
            maxHp = GlobalEnemyStats.instance.maxHp;
            gold = GlobalEnemyStats.instance.gold;
        }

        currentHp = maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        checkHp();
    }

    public void takeDamage(float dmgTaken)
    {
        currentHp -= dmgTaken;
    }

    public void checkHp()
    {
        if (currentHp <= 0)
        {
            PlayerStats.instance.addGold(gold); // add gold to player if this dies
            WaveManager.instance.mobAlive--;
            Destroy(this.gameObject);
        }
    }
}
