using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalEnemyStats : MonoBehaviour
{
    public static GlobalEnemyStats instance;

    public int gold = 30;
    public float maxHp = 100;

    public int previousWave = 1;
    int currentWave;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        currentWave = WaveManager.instance.waveCount;
    }

    private void Update()
    {
        checkWaveCount();
    }

    void checkWaveCount()
    {
        // constantly update value of currentwave with the current wave number in WaveManager
        currentWave = WaveManager.instance.waveCount;

        // if wave has incremented, run this and call an upgrade
        if (previousWave < currentWave)
        {
            upgradeStats();
            previousWave = currentWave; // set previousWave to currentWave
        }
    }
    void upgradeStats()
    {
        // update stats
        Debug.Log("UPDATING STATS");
        maxHp += 20;
        gold += 10;
    }

}
