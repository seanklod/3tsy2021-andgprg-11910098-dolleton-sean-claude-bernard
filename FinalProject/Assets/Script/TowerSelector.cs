using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSelector : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    public TowerStatsBehavior selectedTower;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hit.collider.gameObject.GetComponent<TowerStatsBehavior>())
                {
                    selectedTower = hit.collider.gameObject.GetComponent<TowerStatsBehavior>();
                    selectedTower.gameObject.GetComponent<TowerStatsBehavior>().printTowerInfo();
                }
            }
        }
    }

    public void UpgradeTower()
    {
        selectedTower.gameObject.GetComponent<TowerStatsBehavior>().upgradeTower();
    }
}
