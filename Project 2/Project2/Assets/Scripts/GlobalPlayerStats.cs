using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalPlayerStats : MonoBehaviour
{
    public static GlobalPlayerStats instance;

    public float currentHp;
    public float maxHp;
    public float currentExp;
    public float maxExp;
    public int rank;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
