using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerMgr : MonoBehaviour
{
    public float currentHp;
    public float maxHp;
    public float currentExp;
    public float maxExp;
    public int rank = 1;

    // Start is called before the first frame update
    void Start()
    {
        UIMgr.instance.UpdatePlayerHealth(currentHp, maxHp);
        UIMgr.instance.UpdateExp(currentExp, maxExp);
        UIMgr.instance.UpdateRank(rank);

        // load from global variables
        currentHp = GlobalPlayerStats.instance.currentHp;
        maxHp = GlobalPlayerStats.instance.maxHp;
        currentExp = GlobalPlayerStats.instance.currentExp;
        maxExp = GlobalPlayerStats.instance.maxExp;
        rank = GlobalPlayerStats.instance.rank;
    }

    // Update is called once per frame
    void Update()
    {
        checkExp();
        checkHp();

        // only accessible within death screen
        if (SceneManager.GetActiveScene().name == "DeathScene" || SceneManager.GetActiveScene().name == "Intro")
        {
            // brings player back to scene 1 after dying
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene("Level1");
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            TakeDamage(10);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "EXP")
        {
            earnExp(10.0f);
        }

        if (collider.gameObject.tag == "DeathFloor")
        {
            currentHp = 0;
        }

        if (collider.gameObject.CompareTag("Portal"))
        {
            SavePlayer();
            LoadNextScene();
        }
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void SavePlayer()
    {
        GlobalPlayerStats.instance.currentHp = currentHp;
        GlobalPlayerStats.instance.maxHp = maxHp;
        GlobalPlayerStats.instance.currentExp = currentExp;
        GlobalPlayerStats.instance.maxExp = maxExp;
        GlobalPlayerStats.instance.rank = rank;
    }

    public void TakeDamage(float dmgTaken)
    {
        currentHp -= dmgTaken;

        // clamp HP value to 0
        if (currentHp <= 0) currentHp = 0;
        UIMgr.instance.UpdatePlayerHealth(currentHp, maxHp);
    }

    public void earnExp(float expTaken)
    {
        currentExp += expTaken;
        UIMgr.instance.UpdateExp(currentExp, maxExp);
    }

    public void checkExp()
    {
        if (currentExp >= maxExp)
        {
            if (currentExp > maxExp) currentExp -= maxExp;
            else if (currentExp == maxExp) currentExp = 0;

            maxExp += 10;
            rank++;
            // increase max HP and reset current HP
            maxHp += 20;
            currentHp = maxHp;
        }

        UIMgr.instance.UpdateExp(currentExp, maxExp);
        UIMgr.instance.UpdateRank(rank);
        UIMgr.instance.UpdatePlayerHealth(currentHp, maxHp);
    }

    public void checkHp()
    {
        // if dead, load death scene
        if (currentHp == 0.0f)
        {
            // reset all stats
            GlobalPlayerStats.instance.currentHp = 150;
            GlobalPlayerStats.instance.maxHp = 150;
            GlobalPlayerStats.instance.maxExp = 100;
            GlobalPlayerStats.instance.currentExp = 0;
            GlobalPlayerStats.instance.rank = 1;

            SceneManager.LoadScene("DeathScene");
        }
    }
}
