using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform nozzle;
    public GameObject bulletPrefab;

    public float shotInterval = 0.5f;
    private float shotTime = 0.0f;

    // audio objects
    public AudioClip audioClip;
    public AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((Time.time - shotTime) > shotInterval)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        shotTime = Time.time;

        if (Input.GetKey(KeyCode.Mouse0))
        {
           Instantiate(bulletPrefab, nozzle.transform.position, transform.rotation);
           audioSource.PlayOneShot(audioClip);
        }
    }
}
