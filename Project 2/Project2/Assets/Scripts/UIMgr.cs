using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIMgr : MonoBehaviour
{
    public static UIMgr instance;

    public Image hpBar;
    public TextMeshProUGUI hpText;
    public Image expBar;
    public TextMeshProUGUI expText;
    public TextMeshProUGUI rankText;

    private void Awake()
    {
        instance = this;
    }

    public void UpdatePlayerHealth(float currentHp, float maxHp)
    {
        hpBar.fillAmount = currentHp / maxHp;
        hpText.text = currentHp + " / " + maxHp;
    }

    public void UpdateExp(float currentExp, float maxExp)
    {
        expBar.fillAmount = currentExp / maxExp;
        expText.text = currentExp + " / " + maxExp;
    }

    public void UpdateRank(int rank)
    {
        rankText.text = "Rank: " + rank;
    }

    public void updateLevel(int level)
    {
        //code
    }
}
