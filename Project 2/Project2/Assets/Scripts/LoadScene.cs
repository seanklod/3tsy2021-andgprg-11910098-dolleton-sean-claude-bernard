using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadScene : MonoBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            LoadNextScene();
        }

    }


    // Update is called once per frame
    void Update()
    {
        // only accessible within death screen
        if (SceneManager.GetActiveScene().name == "DeathScene" || SceneManager.GetActiveScene().name == "Intro")
        {
            // brings player back to scene 1 after dying
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene("Level1");
            }
        }
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
