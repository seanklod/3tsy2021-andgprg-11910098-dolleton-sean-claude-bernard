using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AsteroidCollision : MonoBehaviour
{
    public int hp = 50;

    // setting up sfx for death
    public AudioClip audioClip;
    public AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Asteroid"))
        {
            hp--;

            if (hp <= 0)
            {
                AudioSource.PlayClipAtPoint(audioClip, new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y), 1.0f);

                // load death screen after losing all hp
                SceneManager.LoadScene("DeathScreen");
            
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
