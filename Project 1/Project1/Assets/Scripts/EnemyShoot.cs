using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public Transform nozzle;
    public GameObject bulletPrefab;

    public float shotInterval = 0.5f;
    private float shotTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((Time.time - shotTime) > shotInterval)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        shotTime = Time.time;
        Instantiate(bulletPrefab, nozzle.transform.position, transform.rotation);
    }
}
