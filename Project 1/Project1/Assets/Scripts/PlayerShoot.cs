using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform nozzle;
    public Transform nozzle2;

    public GameObject bulletPrefab;


    private int selectedBullet = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            selectedBullet = 1;
        }

        Shoot();
    }

    void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Space) && selectedBullet == 1)
        {
           Instantiate(bulletPrefab, nozzle.transform.position, transform.rotation);
           Instantiate(bulletPrefab, nozzle2.transform.position, transform.rotation);
        }
    }
}
