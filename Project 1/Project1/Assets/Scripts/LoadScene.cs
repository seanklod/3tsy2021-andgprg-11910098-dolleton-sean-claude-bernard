using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            LoadNextScene();
        }

    }


    // Update is called once per frame
    void Update()
    {
        // only accessible within death screen
        if (SceneManager.GetActiveScene().name == "DeathScreen")
        {
            // brings player back to scene 1 after dying
            if (Input.GetKeyDown(KeyCode.A))
            {
                SceneManager.LoadScene(0);
            }
        }
        
    }
}
