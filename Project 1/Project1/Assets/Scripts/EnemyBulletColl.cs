using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyBulletColl : MonoBehaviour
{
    public int hp = 3;

    // setting up sfx for death
    public AudioClip audioClip;
    public AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EBullet"))
        {
            if (this.gameObject.name == "Player")
            {
                hp--;

                if (hp <= 0)
                {
                    AudioSource.PlayClipAtPoint(audioClip, new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y), 1.0f);
                    SceneManager.LoadScene("DeathScreen");
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
