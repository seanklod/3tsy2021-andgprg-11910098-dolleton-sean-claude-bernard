using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLineSFXTrigger : MonoBehaviour
{
    public AudioClip audioClip;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            audioSource.PlayOneShot(audioClip);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
