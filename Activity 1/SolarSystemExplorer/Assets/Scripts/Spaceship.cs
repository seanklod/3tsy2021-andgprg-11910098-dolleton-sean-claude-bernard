using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{
    int movementSpeed = 100;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    void Movement()
    {
        // move right
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.position += new Vector3(movementSpeed, 0, 0) * Time.deltaTime;
        }

        // move left
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.position -= new Vector3(movementSpeed, 0, 0) * Time.deltaTime;
        }

        // move forward
        if (Input.GetKey(KeyCode.W))
        {
            this.transform.position += new Vector3(0, 0, movementSpeed) * Time.deltaTime;
        }

        // move back
        if (Input.GetKey(KeyCode.S))
        {
            this.transform.position -= new Vector3(0, 0, movementSpeed) * Time.deltaTime;
        }

    }
}
