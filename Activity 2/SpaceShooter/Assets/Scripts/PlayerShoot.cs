using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform nozzle;
    public Transform nozzle2;

    public GameObject bulletPrefab;
    public GameObject bulletPrefab2;
    public GameObject bulletPrefab3;
    public GameObject bulletPrefab4;

    private int selectedBullet = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            selectedBullet = 1;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            selectedBullet = 2;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            selectedBullet = 3;
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            selectedBullet = 4;
        }

        Shoot();
    }

    void Shoot()
    {
        if (Input.GetKey(KeyCode.Space) && selectedBullet == 1)
        {
           Instantiate(bulletPrefab, nozzle.transform.position, transform.rotation);
           Instantiate(bulletPrefab, nozzle2.transform.position, transform.rotation);
        }

        if (Input.GetKey(KeyCode.Space) && selectedBullet == 2)
        {
           Instantiate(bulletPrefab2, nozzle.transform.position, transform.rotation);
           Instantiate(bulletPrefab2, nozzle2.transform.position, transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Space) && selectedBullet == 3)
        {
            Instantiate(bulletPrefab3, nozzle.transform.position, transform.rotation);
            Instantiate(bulletPrefab3, nozzle2.transform.position, transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Space) && selectedBullet == 4)
        {
            Instantiate(bulletPrefab4, nozzle.transform.position, transform.rotation);
            Instantiate(bulletPrefab4, nozzle2.transform.position, transform.rotation);
        }
    }
}
